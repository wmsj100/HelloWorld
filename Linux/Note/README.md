---
title: README.md
date: 2018-03-08 19:46:02 Thu
tag: [readme]
categories: Linux
author: wmsj100
mail: wmsj100@hotmail.com
---

#README
> 千里之行，始于足下

##目录概要
- 这是关于linux的系统知识点的记录

##目录详情
- Static 静态知识点，就是概念性质的知识点，不设计处理文本
- Dynamic 动态的知识点，主要是进行文本处理，有交互过程
- TextFlow 文档编辑，这里是linux的三剑客sed,awk,grep

##代办事项
~~None~~
