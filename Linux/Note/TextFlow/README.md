---
title: README
date: Thu 08 Mar 2018 06:29:56 PM CST
tag: [readme,tools]
categories: Linux
author: wmsj100
mail: wmsj100@hotmail.com
---

# README

## 目录概要
- 这里是文档处理三剑客，是处理文档的核心知识点

## 目录详情
- AWK 以列为处理单位，有单独的编程语言，生成报表
- SED 以行为处理单位，是很强大的流处理工具
- GREP 以行为单位，找出目标所在的行的信息
