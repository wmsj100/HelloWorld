---
title: xWindows 窗口解析
date: Sun 04 Feb 2018 06:57:04 PM CST
tag: [Linux]
categories: Linux
author: wmsj100
mail: wmsj100@hotmail.com
---

- 视窗主要由主管硬件的‘xservice'和主管运算的'xclient'以及管理各个'xclient'的’window manager' 组成
- [xwindows解析，鸟哥的私房菜](http://linux.vbird.org/linux_basic/0590xwindow.php)
