---
title: README
date: Thu 08 Mar 2018 06:02:13 PM CST
tag: [readme]
categories: Linux
author: wmsj100
mail: wmsj100@hotmail.com
---

# README

## 目录概要
- 这里是关于linux的系统方面的资料收藏，包括系统常用软件`Software`，图片收藏`Img`，项目收藏`Project`

## 目录详情

- Img 关于Linux的一些图片整理目录
- Project 关于Linux的项目整理目录
- SoftWare 收藏的一些好用的Linux软件目录
