---
title: README
date: 2018-03-05 11:12
modify: 2019-03-06 23:15:05	
tag: [linux,readme]
categories: Linux
author: wmsj100
mail: wmsj100@hotmail.com
---

# README

## 目录概要

## 目录详情
- Note 基础知识点目录
- Summary 我自己关于Linux的一些总结目录
- Storage 关于linux的系统的收藏，包括软件/项目/图片等
- Func 介绍linux系统常用的函数的功能
- SoftWare Linux好用的软件
- Shell shell脚本
- Services 云系统和网络

## 注意事项
~~None~~
