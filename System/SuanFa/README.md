---
title: README
date: 2019-04-29 17:09:09	
modify: 
tag: [readme]
categories: SuanFa
author: wmsj100
mail: wmsj100@hotmail.com
---

# 算法

## 概述
- 算法是一步一步解决问题或完成任务的方法
- 是一组明确明确步骤的有序集合，
- 它产生结果并在有限时间内终止
- 算法是通用的，语言无关的
- 这里整理常见算法

## 目录
- Sort 排序算法
- Find 查找算法
- DiGui 迭代和递归
