---
title: README
date: 2019-04-29 15:09:09	
modify: 
tag: [readme]
categories: SuanFa
author: wmsj100
mail: wmsj100@hotmail.com
---

# 递归

## 概述
> 递归和迭代类似，但递归很容易理解，迭代速度更快
- 递归: 调用自身，且朝着临界条件靠拢的趋势发展，递归之所以性能不好是因为所有的子函数和局部变量都要在栈中展开，而栈存储是有限的，会造成性能问题
- 迭代: 通过循环来实现递归的操作
