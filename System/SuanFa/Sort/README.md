---
title: README
date: 2019-04-29 17:09:09	
modify: 
tag: [readme,sort]
categories: SuanFa
author: wmsj100
mail: wmsj100@hotmail.com
---

# 排序

## 概述
> 常见的排序算法有下面三种，虽然他们是效率最低的，到这是所有高级排序算法的基础
- 原则排序
- 冒泡排序
- 插入排序
