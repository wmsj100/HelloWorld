---
title: README
date: 2019-04-29 17:09:09	
modify: 
tag: [readme]
categories: SuanFa
author: wmsj100
mail: wmsj100@hotmail.com
---

# 查找

## 概述
> 常见的查找有下面俩种方法
- 顺序查找: 适用于无序列表
- 折半查找: 适用于排序列表
