---
title: README
date: 2019-04-09 17:56:29	
modify: 
tag: [readme]
categories: HardWare
author: wmsj100
mail: wmsj100@hotmail.com
---

# README

## 概述
- 这是嵌入式的知识点收集，通过嵌入式来学习编程会更有乐趣，而且可以加深理解一些习以为常的事情，而且通过操作硬件可以实现很多玩法

## 目录
- RaspberryPi 树莓派，当前最流行的微型电脑，打算入手一台，可以当作微型服务器，实现很多种功能
- microBit	BBC联合其他厂商实现的硬件，支持python编程 [官网](https://microbit.org/zh-CN/code/)

## 参考
- [microBit](https://microbit.org/zh-CN/code/)
