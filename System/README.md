---
title: README
date: 2019-04-09 17:09:09	
modify: 
tag: [readme]
categories: Basic
author: wmsj100
mail: wmsj100@hotmail.com
---

# README

## 概述
- 这是关于操作系统以及硬件的知识

## 目录
- Network: 关于网络的知识点
- DataStract: 关于数据结构
- RTOS: 实时操作系统，在规定时间内返回结果
- SuanFa 常见计算机算法

