---
title: README
date: 2018-04-01 10:18:56 Sun
modify: 2018-04-01 10:18:56 Sun
tag: [readme]
categories: Linux
author: wmsj100
mail: wmsj100@hotmail.com
---

# README

## 目录概要
- 该目录主要是关于Windows的知识点整理！
- Windows的市场份额在全球占有绝对领导地位，不管是概要统计还是分析调研[参考文章](https://article.pchome.net/content-149721-4.html)
- 主要应用与中低端服务器市场
- 基本操作必须要掌握

## 目录详情
- Note 基础知识点的整理目录

## 代办事项
~~None~~
