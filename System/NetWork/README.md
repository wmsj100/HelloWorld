---
title: README
date: Thu 08 Mar 2018 05:51:50 PM CST
tag: [readme]
categories: Network
author: wmsj100
mail: wmsj100@hotmail.com
---

# README

## 网站收藏
- [网中人](http://www.study-area.org/network/networkfr.htm)
- [鸟哥的私房菜 服务器篇](http://cn.linux.vbird.org/linux_server/0105beforeserver_2.php#server_network_flow)

## 目录概要
- 这里是关于网络的知识点，包括硬件和软件知识

## 目录详情
- Note 关于网络的基础知识点
- Storage 关于网络的资料收藏
