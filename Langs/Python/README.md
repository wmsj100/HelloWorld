---
title: README
date: 2018-04-13 22:05:37 Fri
modify: 2018-04-13 22:05:37 Fri
tag: [readme]
categories: Python
author: wmsj100
mail: wmsj100@hotmail.com
---

# README
> 学习python时，如果遇到困境，试着输入`import this`

## 概要
- 该目录主要是关于python的知识点整理！

## 详情
- Note 基础知识点的整理目录
- Frame python的框架
- Article 关于python的文章
- Code 代码收藏

## 网站
- [python学习资料](https://my.oschina.net/u/3887904/blog/1842003)
