---
title: README
date: 2018-04-13 22:02:20 Fri
modify: 2018-04-13 22:02:20 Fri
tag: [django,readme]
categories: Django
author: wmsj100
mail: wmsj100@hotmail.com
---

# README

## 概要
- 该目录主要是关于Django的知识点整理！
- `Django_book_2` 最好的入门书籍，不知道是不是书名，很受用，感谢
- [Django1.11文档](https://yiyibooks.cn/xx/Django_1.11.6/intro/overview.html)
- [Django入门介绍](https://yiyibooks.cn/xx/Django_1.11.6/intro/whatsnext.html)
- [Django官方文档](https://docs.djangoproject.com/zh-hans/2.0/)
- [Django-入门](http://www.cnblogs.com/luxiaojun/p/5791498.html)
- [Django-models教程](https://www.cnblogs.com/luxiaojun/p/5795070.html)

## 详情
- Note 基础知识点的整理目录

## 代办
- sqlite3 安装完成，可以搭建自己的blog了  -- Fri Apr 20 00:12:38 CST 2018
- 学习的最好方法就是开始一个项目 -- Fri May  4 16:23:12 CST 2018
