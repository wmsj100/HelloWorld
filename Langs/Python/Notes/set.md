---
title: set集合
date: 2019-04-09 17:22:51	
modify: 
tag: [basic]
categories: Python 
author: wmsj100
mail: wmsj100@hotmail.com
---

# set集合

## 概述
- 集合的特征是内部没有重复的值

## 使用
- set: 通过关键字`set`来创建一个集合
	- `a = set([1,2,3])`
- `add`: 通过`add`向集合中添加值
- `remove`: 通过`remove`来删除集合中的值
- 集合只能整体读取，不能通过下标的方式来读取某个值
- len(set_value) 通过`len`可以获取到集合的长度
- for：集合可以通过`for`循环来进行值的遍历

## 参考
- [Python的常用数据结构](https://segmentfault.com/ls/1650000017333471/l/1500000016653718)
