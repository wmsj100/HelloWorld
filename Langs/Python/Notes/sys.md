---
title: sys模块
date: Sun 31 Dec 2017 11:46:08 AM CST
tag: [python]
categories: Linux
author: wmsj100
mail: wmsj100@hotmail.com
---

- sys 这个模块主要是获取脚本文件后面的值
- sys.argv 获取到的时一个数组，
    - sys.argv[0] == 文件名
    - sys.argv[1] 是文件名后面的值
