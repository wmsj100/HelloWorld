---
title: pip
date: 2018-03-18 22:38:10 Sun
modify: 2018-03-18 22:38:10 Sun
tag: [pip,python,tool]
categories: Python
author: wmsj100
mail: wmsj100@hotmail.com
---

# pip python软件管理工具

## 软件安装
- https://pypi.python.org/packages/e5/8f/3fc66461992dc9e9fcf5e005687d5f676729172dda640df2fd8b597a6da7/pip-9.0.2.tar.gz#md5=2fddd680422326b9d1fbf56112cf341d 下载pip软件包
- tar -zxvf pip.tar.gz
- sudo python setup.py install 安装pip
- pip -V 查看pip版本号
