---
title: 标签tag
date: Sun 25 Feb 2018 03:29:04 PM CST
tag: [git,basic]
categories: git
author: wmsj100
mail: wmsj100@hotmail.com
---

# git标签

## 基础
- git tag || git tag -l 查看当前git库的标签列表
- git tag -n 查看当前所有标签的信息描述
- git tag -a v2.0.0 -m '版本v2.0'
- git push --tags || git push origin --tags 推送本地所有标签到服务器
- git tag -d tagName 删除tagName 标签名称
