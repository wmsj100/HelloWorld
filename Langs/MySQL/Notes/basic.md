# 基础知识

## 字符型
- select 'hello', '""hello""', "''hello''", '\'hello';

## 布尔值
- true等于1
- false等于0
- true和false大小写不区分
