---
title: README
date: 2018-03-05
modify: 2019-04-21 21:55:29	
tag: linux,editor,tool
categories: Linux
author: wmsj100
mail: wmsj100@hotmail.com
---

# README

## 概述
- 这是接触过大额语言和好用的工具的知识点整理

## 目录详情
- Scratch 一个积木式的编程语言
- Abandon 之前接触过的语言
- C C语言
- Git 版本控制系统工具`git`的知识点整理
- MySQL 数据库
- Python Python语言
- Shell Linux的Shell语言
- SQLite 嵌入式的数据库
- Vim vim编辑器，所有类Unix系统默认安装的编辑器
- VsCode IDE
- WebUI 前端知识

## 软件收藏
- [Sumatra PDF](https://www.sumatrapdfreader.org/downloadafter.html)
