---
title: README 
date: 2018-06-24 13:16:51	
modify: 
tag: [readme]
categories: WebUI
author: wmsj100
mail: wmsj100@hotmail.com
---

# README

## 概述
- 这是前端的知识体系
- 之前是基于`js`展开的学习，而且以基础语法为主，使用到的插件很少
- 现在转型到`TS`，围绕ts的生态来展开学习，要大量使用到插件，
- 现在以工程为主

## 目录解析
- Basic 基础的前端知识，基于js的知识体系；
- Typescript ts的知识点，包含基础语法以及基于ts的框架，如angular；
- Node node的知识体系，ts的很多操作的编译过程是在node中完成的。
- ES6 ECMAScript6语法基础知识
- Angular2 关于Angular2的知识点

## 备注
- 反反复复选择中最后还是在运维团队中做着前端的事情,到现在才意识到还是更倾向于前端.
- 喜欢创造者的角色,而写代码就是在创生.
- 这一次是自己主动争取来的机会,想要好好学习整个框架,提升自己的编码水平

## 技术栈
- Yarn[官网](https://yarnpkg.com/zh-Hans/) 能替代`npm`的包管理器

## API
> 支持跨域获取数据的api
- [精品API](https://www.cnblogs.com/trackingmore/p/7156877.html)
- [https://www.swapi.co/](https://www.swapi.co/) 获取的是星球大战的数据
