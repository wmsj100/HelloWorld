---
title: 关于前端的思考
date: 2018-05-11 23:03:08 Fri
modify: 2018-05-11 23:03:08 Fri
tag: [diary]
categories: WEB
author: wmsj100
mail: wmsj100@hotmail.com
---

# 关于前端的思考

## 正文
- 之前一直想要逃离前端，感觉前端太狭窄了，不是我想要做的事情，但是来到运维的这俩个月的经历，我发现还是更喜欢写代码逻辑。
- 如果你说不行，我反倒是要挑战一下，只是想要证明自己是可以做到的。
- 前端有前端的门槛，有前端的邻域，
- 现在在这边一个人做一个项目，因为对于后端不感知，所以我反倒是更倾向于在前端解决事情，比如说过滤，或者排序，因为内容不是很多，暂时就不考虑性能问题了。
- 想要好好学习前端了，这正好是我熟悉的领域，也是我很陌生的邻域，可以学习动画，学习H5，学习新的模块写法。
