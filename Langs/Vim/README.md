---
title: README.md
date: 2018-05-06 12:30:47 Sun
modify: 2018-05-06 12:30:47 Sun
tag: []
categories: Linux
author: wmsj100
mail: wmsj100@hotmail.com
---

# README

## 详情
- Note 基础知识点的整理目录

## 记录
- [vim配置](http://www.cnblogs.com/witcxc/archive/2011/12/28/2304704.html)
- [vim 改造IDE](http://blog.csdn.net/wooin/article/details/1858917)
- [跟我一起学vim](http://blog.csdn.net/mergerly/article/details/51671890)
- [Bundle插件管理](http://blog.csdn.net/jiaolongdy/article/details/17889787)
- [插件网站](https://vimawesome.com/plugin/emmet-vim)

## 代办
~~None~~

