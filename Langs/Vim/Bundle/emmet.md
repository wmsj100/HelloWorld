---
title: emmet
date: 2018-05-06 17:55:34 Sun
modify: 2018-05-06 17:55:34 Sun
tag: [frame]
categories: VIM
author: wmsj100
mail: wmsj100@hotmail.com
---

# emmet

## 概述
- 该插件的所有操作都是基于ctrl+y+， 组合

## 范例
- div>ul>li*3>span ctrl+y+,
- shift+v ul>li*3*
