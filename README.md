---
title: README
date: 2018-04-05 09:03:43 Thu
modify: 2019-04-09 17:08:44	
tag: [readme]
categories: Basic
author: wmsj100
mail: wmsj100@hotmail.com
---

# README
> 不积跬步无以至千里

## 概要
- 该目录主要是基础设施的知识整理！

## 详情
- Linux Linux系统知识点
- Langs 学习过的语言
- System 关于操作系统的知识点

## 书单
### 操作系统
- [计算机科学导论]  这本书被称为是神级计算机书籍，今天看了几个章节确实如此，深入浅出的概念介绍

### C
- [C编程中文网]  C的入门教程，完全凭借它的教程有了对C的理解
- [C羊皮书]  13年后面的一本书，19年4月份第一次系统过了一遍，收获挺多

## 技术路线
- C/C++ 嵌入式编程
- Linux系统编程
- python自动化编程以及基于它的框架
- Open Stack 云操作系统
- 计算机网络编程

## 学习网址
- [中国大学MOOC](https://www.icourse163.org) 这是网易搭建的大学公开课
